
require File.expand_path('../app/helpers/template_helper', __FILE__)
require_dependency 'helpers/issue_helper'
require_dependency 'hooks/view_issues_show_details_bottom_hook_listener'
require_dependency 'hooks/controller_issues_edit_after_save_hook_listener'
require_dependency 'hooks/controller_issues_bulk_edit_before_save_hook_listener'

Redmine::Plugin.register :redmine_batch_subtask_generation do
  name 'Redmine Batch Subtask Generation plugin'
  author 'balx'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url '#'
  author_url '#'
  menu :admin_menu, \
      :redmine_batch_subtask_generation, \
      { controller: 'template', action: 'index' }, \
      caption: 'Subtasks template', \
      :html => {:style => 'background-image: url(../plugin_assets/redmine_batch_subtask_generation/images/subtasks_template.png); background-repeat: no-repeat;padding-left: 20px'}, \
      last: true
  # project_module :subtask_template do
  #   permission :view_template, template: :index
  #   permission :create_template, template: :index
  #   permission :edit_template, template: :edit
  # end
end
