
var GenerateSubtasks = function () {

    // Default setting
    this.acceptTrackers = [];
    this.defaultTemplate = [];
    this.template = [];

    this.addedIssues = [];

    this.init = function () {
        
    }

    this.renderTemplatesAsDetail = function (templates, $ulEle, trackers, subject) {
        for (let i in templates) {
            let template = templates[i];
            let liEle = `<li class="item"><p><a href="#">${trackers[template['tracker_id']]}: </a>${subject}</p><ul class="list"></ul></li>`;
            $liEle = $(liEle);
            $ulEle.append($liEle);
            $(liEle).find('a').on('click',function (e) {
                e.preventDefault();
            })
            
            if (template['children'] != undefined && template['children'].length > 0) {
                this.renderTemplatesAsDetail(template['children'], $liEle.find('> ul'), trackers, subject);
            }
        }
    }

    this.getTemplateOptions = function (trackerId, templates) {
        templates = this.filterTemplateBytracker(trackerId, templates);
        let option = '<option value=""></option>';
        if (templates.length > 0) {
            templates.map(template => {
                option += `<option value="${template.id}">${template.name}</option>`;
            });
        }
        return `<select>${option}</select>`;
    }

    this.filterTemplateById = function (id, templates) {
       return templates.filter(template => template.id == id);
    }

    this.filterTemplateBytracker = function(currentTrackerId, templates) {
        return templates.filter(template => template.tracker == currentTrackerId);
    }

    this.setDefaultSetting = function (defaultTemplate) {
        if (Object.keys(defaultTemplate).length > 0) {
            this.defaultTemplate = defaultTemplate.structure;
            this.template = defaultTemplate.structure;
            this.acceptTrackers.push(defaultTemplate.tracker);
        } else {
            this.defaultTemplate = [];
            this.template = [];
            this.acceptTrackers = [];
        }
    }

    this.handleClickGenerateSubtasks = function () {
        if (this.defaultTemplate.length == 0) {
            return;
        }
        return this.checkAllowGenerate()
                .then(isAllow => {
                    if (isAllow) {
                        return this.createChildrenIssues(this.template, this.getCurrentIssueId());
                    } 
                    return false;
                })
                .then(() => {
                    location.reload();
                })
                .catch((e) => {
                    setCookie('redmine_api_key', '');
                    this.deleteIssuesIfError();
                    this.displayError();
                    console.log(e);
                });
    }

    /**
     * Check tracker to decide allow click button "Generate" or not
     */
     this.checkAllowGenerate = async function () {

        let data = await apiGetIssue(this.getCurrentIssueId());
        if (!data || !data.issue) {
            return false;
        }

        let trackerId = data.issue.tracker.id;
        if (!this.acceptTrackers.includes(trackerId.toString())) {
            return false;
        } 

        this.template = await this.makeTemplate();
        return this.template.length > 0;
    }

    this.checkDisplayButtonGenSubtasks = function (currentTrackerId, templates, btnGenSubtasks) {
        if (this.filterTemplateBytracker(currentTrackerId, templates).length > 0) {
            btnGenSubtasks.removeClass('hide');
        } else {
            btnGenSubtasks.addClass('hide');
        }
    }

   

    /**
     * Create children issues
     * 
     * @param {*} issues 
     */
    this.createChildrenIssues = async function (issues, parentIssueId) {

        for (let i = 0; i < issues.length; i++) {
            let parentId = (issues[i]['parent_issue_id'] != undefined && issues[i]['parent_issue_id'] != '') 
                            ? issues[i]['parent_issue_id'] : parentIssueId;
            let childIssue = this.setChildIssueInfo(issues[i]['tracker_id'], parentId);
            // Call api create issue
            let data = await apiCreateIssue(childIssue);
            this.addedIssues.push(data.issue.id);
            if (Object.keys(issues[i]).includes('children')) {
                await this.createChildrenIssues(issues[i]['children'], data.issue.id);
            }
        }
        return true;
    }

    /**
     * Add extra info for create child issue
     * 
     * @param {*} trackerId 
     * @param {*} parentIssueId 
     * @returns object
     */
    this.setChildIssueInfo = function (trackerId, parentIssueId) {
        let issue = {};

        if (assignedToId) {
            issue['assigned_to_id'] = assignedToId;
        }
        if (trackerId) {
            issue['tracker_id'] = trackerId;
        }
        if (subject) {
            issue['subject'] = subject.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, "\"").replace(/&#39;/g, "'");;
        }
        if (projectId) {
            issue['project_id'] = projectId;
        }
        if (priorityId) {
            issue['priority_id'] = priorityId;
        }
        if (fixedVersionId) {
            issue['fixed_version_id'] = fixedVersionId;
        }
        if (parentIssueId) {
            issue['parent_issue_id'] = parentIssueId;
        }
        issue['is_generate'] = true;
        // if (startDate) {
        //     issue['start_date'] =  formatDate(startDate);
        // }
        return { issue };
    }



    /**
     * Delete issues if error
     * 
     * @returns void
     */
    this.deleteIssuesIfError = function () {
        if (this.addedIssues.length == 0) {
            return;
        }
        this.addedIssues.map((issueId) => {
            apiDeleteIssue(issueId)
                .then(() => {
                    this.addedIssues = this.addedIssues.filter(val => val != issueId);
                });
        });
    }



    /**
     * Get template for create children issues
     * 
     * @returns array
     */
    this.makeTemplate = async function () {
        let data = await apiGetIssueAndChildrens(this.getCurrentIssueId());
        if (!data || !data.issue || data.issue.children == undefined || data.issue.children.length == 0) {
            return this.defaultTemplate;
        }
        this.template = this.diffTemplates(data.issue.children, this.defaultTemplate, data.issue.id);
        return this.template;
    }


    this.diffTemplates = function (dbTemplates, clientTemplates, dbParentIssueId) {
        let diffTemplates = [];
        clientTemplates.map((clientChildTemplates) => {
            let isExist = false
            dbTemplates.map(dbChildTemplates => {
                if (dbChildTemplates.tracker.id == clientChildTemplates.tracker_id) {
                    isExist = true;

                    // Exist, check children
                    if (clientChildTemplates.children != undefined && clientChildTemplates.children.length > 0) {
                        if (dbChildTemplates.children == undefined) {
                            diffTemplates = diffTemplates.concat(this.customTemplate(clientChildTemplates.children, dbChildTemplates.id));
                        } else {
                            diffTemplates = diffTemplates.concat(this.diffTemplates(dbChildTemplates.children, clientChildTemplates.children, dbChildTemplates.id));  
                        }
                    }
                }
            });
            if (!isExist) {
                diffTemplates = diffTemplates.concat(this.customTemplate([clientChildTemplates], dbParentIssueId));
            }
        });
        return diffTemplates;
    }



    this.customTemplate = function(clientTemplates, dbParentIssueId) {
        let results = [];
        clientTemplates.map((template) => {
            // prevent reference
            let tmp = {
                tracker_id: template.tracker_id
            }
            if (template.children != undefined) {
                tmp['children'] = template.children;
            }
            if (dbParentIssueId) {
                tmp['parent_issue_id'] = dbParentIssueId;
            }
            results.push(tmp);
        });
        return results;
    }

    this.getCurrentIssueId = function () {
        var url = window.location.href;
        var m = /issues\/(\d+)/.exec(url);
        if (m) {
            return m[1];
        }
        return null;
    }


    this.displayError = function () {
        let contentEle = $('#content');
        let errorEle = $('#flash_error');
        let errorMsg = 'An error occured!';
        if (errorEle.length == 0) {
            contentEle.prepend('<div class="flash error" id="flash_error">' + errorMsg + '</div>')
        } else {
            errorEle.html(errorMsg);
        }
    }

    this.init();
}
