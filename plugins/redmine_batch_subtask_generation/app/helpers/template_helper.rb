module TemplateHelper

    TEMPLATE_PATH = "plugins/redmine_batch_subtask_generation/templates/";
    
    def get_template_by_id(id)
        templates = get_templates()
        templates.each do |template| 
        return template if template['id'].to_s == id.to_s
        end
        return {}
    end

    def write_json(contents)
        contents = contents.empty? ? [] : contents
        File.open(TEMPLATE_PATH + "template.json","w") do |f|
        f.write(JSON.pretty_generate(contents))
        end 
    end

    def get_templates
        file = File.read(TEMPLATE_PATH + 'template.json')
        file = file.empty? ? "[]" : file
        return JSON.parse(file)
    end

    def generate_template()
        template_data = params[:template]
        template = {
            "id": generate_id(template_data['name']),
            "created": Time.now.strftime("%Y/%m/%d"),
            "name": template_data['name'],
            "tracker": template_data['tracker'],
            "structure": JSON.parse(template_data['structure'])
        }
    end

    def generate_id(name)
        return '' if name.empty?
        name.squish.downcase.tr(" ","_")
    end

    def get_trackers() 
        results = {}
        trackers = Tracker.all.to_a
        trackers.each do |tracker| 
            results.store(tracker['id'], tracker['name'])
        end
        return results
    end

    def validate_create 
        template_data = params[:template]
        template_file_contents = get_templates()
        error_msg = ''
        id = generate_id(template_data['name']);
        if id.empty?
            error_msg += 'Name can\'t blank! '
        else
            template_file_contents.each do |template|
                if id.to_s == template['id'].to_s
                    error_msg += 'Name already existed!'
                end
            end
        end
        return error_msg
    end

    def validate_edit
        template_data = params[:template]
        template_file_contents = get_templates()
        error_msg = ''
       
        id = generate_id(template_data['name']);
        if id.empty?
            error_msg += 'Name can\'t blank! '
        else 
            name_cnt = 0
            template_file_contents.each do |template|
                if params[:template_id].to_s == template['id'].to_s \
                    || id.to_s == template['id'].to_s
                    name_cnt += 1
                end
            end
            if name_cnt > 1
                error_msg += 'Name already existed!'
            end
        end
        return error_msg
    end
end
