module IssueHelper

    TEMPLATE_PATH = "plugins/redmine_batch_subtask_generation/templates/";
    
    def updateChildIssues(context)
        updateFields = getFieldsWillUpdate(context)
        return true if updateFields.empty?

        @issue = context[:issue]
        dataUpdate = getDataUpdateChildIssues(@issue.id, @issue, updateFields)
        begin
          Issue.update(dataUpdate.keys, dataUpdate.values) unless dataUpdate.empty?
        rescue Exception => error
            return false
        end
        true
    end

    # Get data for update child issues
    def getDataUpdateChildIssues(parentIssueId, issue, updateFields)
        @dataUpdate = {}
        childIssues = Issue.where(["parent_id = ?", parentIssueId]).select("id")
        unless childIssues.empty? 
            childIssues.each do |child|
                tmp = {}
                updateFields.each do |field|
                    if updateFields.include?(field)
                        tmp.store(field, issue[field])
                    end
                end 

                @dataUpdate[child.id] = tmp
                @dataUpdate = @dataUpdate.merge!(getDataUpdateChildIssues(child.id, issue, updateFields))
            end
        end
        @dataUpdate
    end

    def getFieldsWillUpdate (context)
        updateFields = [
             #"fixed_version_id"
        ]
        return updateFields;

        # results = []

        # newIssue = context[:request]['issue']
        # oldIssue = Issue.find(context[:issue].id)
        # updateFields.each do |field|
        #     # don't need update if value doesn't change 
        #     unless  oldIssue[field] ==  newIssue[field]
        #         results.push(field)
        #     end
        # end
        # return results
    end

end