module ProjectHelper
    def save_copy_setting(context)
        project_id = context[:project][:id]

        old_records =  CopySetting.where(["project_id = ?", project_id]).select(&:id)
        for rec in old_records
            rec.destroy
        end
        copy_setting = context[:params][:issue_copy_setting_ids]
        if copy_setting != nil
            arr_allow_copy = copy_setting[:allow_copy]
            arr_is_default = copy_setting[:is_default]

            if arr_allow_copy != nil
                for custom_field_id in arr_allow_copy
                    if arr_is_default != nil and arr_is_default.include? custom_field_id
                        CopySetting.create(custom_field_id: custom_field_id, project_id: project_id, allow: true, is_default: true)
                    else
                        CopySetting.create(custom_field_id: custom_field_id, project_id: project_id, allow: true, is_default: false)
                    end
                end
            end
        end

        category_copy_setting = context[:params][:issue_category_copy_setting_ids]
        old_records = CategoryCopySetting.where(["project_id = ?", project_id]).select(&:id)
        for rec in old_records
            rec.destroy
        end
        if category_copy_setting != nil
            if category_copy_setting[:allow_copy] != nil
                if category_copy_setting[:is_default] != nil
                    CategoryCopySetting.create(project_id: project_id, allow: true, is_default: true)
                else
                    CategoryCopySetting.create(project_id: project_id, allow: true, is_default: false)
                end
            end
        end
    end
end