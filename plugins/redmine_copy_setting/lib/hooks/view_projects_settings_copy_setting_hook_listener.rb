class ViewProjectsSettingsCopySettingHookListener < Redmine::Hook::ViewListener

  def view_projects_settings_copy_setting(context = {})
    context[:controller].send(:render_to_string, {
      :partial => 'hooks/view_projects_settings_copy_setting',
      :locals => {
        context: context
      },
    })
  end

end