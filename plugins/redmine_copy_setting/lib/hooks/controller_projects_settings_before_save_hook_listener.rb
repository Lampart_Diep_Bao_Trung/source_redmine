class ControllerProjectsSettingsBeforeSave < Redmine::Hook::ViewListener
    include ProjectHelper

    def controller_projects_settings_before_save(context = {})
      save_copy_setting(context)
      true
    end
end