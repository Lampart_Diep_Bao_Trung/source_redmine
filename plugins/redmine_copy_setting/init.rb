require_dependency 'helpers/project_helper'
require_dependency 'hooks/controller_projects_settings_before_save_hook_listener'
require_dependency 'hooks/view_projects_settings_copy_setting_hook_listener'
Redmine::Plugin.register :redmine_copy_setting do
  name 'Redmine Copy Setting plugin'
  author 'Khang NLM'
  description 'This is a plugin for coppy project setting'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'
end
