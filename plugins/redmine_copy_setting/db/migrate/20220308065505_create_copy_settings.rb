class CreateCopySettings < ActiveRecord::Migration[5.2]
  def self.up
    create_table :copy_settings do |t|
      t.integer :custom_field_id, default: 0, null: false
      t.integer :project_id, default: 0, null: false
      t.boolean :allow, default: false, null: false
      t.boolean :is_default, default: false, null: false
    end
  end

  def self.down
    drop_table :copy_settings
  end
end
